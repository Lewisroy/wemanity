Installation

composer update pour mettre à jour symfony 3

crée la bdd Wemanity (root:null)


bin/console d:s:u -f pour mettre la bdd à jour


bin/console doctrine:fixtures:load pour générer des données utilisateurs

(Compte admin a pour mot de passe : admin123)

(Compte user a pour mot de passe : test123)

Mettre les bons paramètres pour la gestion des mails et donc de l'inscription dans app/config/parameters.yml


bin/console c:c pour vider le cache


bin/console s:r pour lancer le serveur

Here we go