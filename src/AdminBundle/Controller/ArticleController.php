<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Datetime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * Article controller.
 *
 * @Route("/{_locale}/blog/article")
 */
class ArticleController extends Controller {

    /**
     * Lists all article entities.
     *
     * @Route("/", name="article_blog_index")
     * @Method("GET")
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('AdminBundle:Article')->findByCriteria(24, 0);

        return $this->render('article_blog/index.html.twig', array(
                    'articles' => $articles,
        ));
    }

    /**
     * Lists 5 articles entities.
     *
     * @Route("/layout", name="article_blog_layout")
     * @Method("GET")
     */
    public function layoutAction() {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('AdminBundle:Article')->findByCriteria(5, 0);

        return $this->render('article_blog/layout.html.twig', array(
                    'articles' => $articles,
        ));
    }

    /**
     * Lists all article entities.
     *
     * @Route("/ajax", name="get_articles_ajax")
     * @Method("POST")
     */
    public function ajaxGetArticleAction(Request $request) {

        $articlesToReturn = array();

        $em = $this->getDoctrine()->getManager();
        $start = $request->request->get('start');
        $number = $request->request->get('number');

        $articles = $em->getRepository('AdminBundle:Article')->findByCriteria($number, $start);
        $locale = $request->getLocale();
        if (strtolower($locale) == 'fr') {
            foreach ($articles as $article) {
                $tmp = array();
                $tmp['name'] = $article->getName();
                $tmp['text'] = substr($article->getText(), 0, 250);
                $tmp['url'] = '<div class="text-center" style="margin-top: 5px;"><a href="' . $this->generateUrl(
                                'article_blog_show', array('id' => $article->getId())) . '" class="be-popup-sign-button menu-cover" style="float: none; padding: 7px 4px" > Voir plus </a></div>';
                $tmp['date'] = $article->getDateOpening()->format('d-m-y h:i');
                $tmp['likers'] = count($article->getLikers());
                $tmp['viewers'] = count($article->getViewers());
                array_push($articlesToReturn, $tmp);
            }
        }
        if (strtolower($locale) == 'en') {
            foreach ($articles as $article) {
                $tmp = array();
                $tmp['name'] = $article->getNameEn();
                $tmp['text'] = $article->getTextEn();
                $tmp['url'] = '<div class="text-center" style="margin-top: 5px;"><a href="' . $this->generateUrl(
                                'article_blog_show', array('id' => $article->getId())) . '" class="be-popup-sign-button menu-cover" style="float: none; padding: 7px 4px" > Read more </a></div>';
                $tmp['date'] = $article->getDateOpening()->format('d-m-y h:i');
                $tmp['likers'] = count($article->getLikers());
                $tmp['viewers'] = count($article->getViewers());
                array_push($response, $tmp);
            }
        }



        return new JsonResponse($articlesToReturn);
    }

    /**
     * Like an article
     * 
     * @Security("is_granted('ROLE_USER')")
     * @Route("/like", name="article_blog_like")
     * @Method("POST")
     */
    public function likeAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $return = array();
        $return['minus_one'] = false;
        $return['plus_one'] = false;
        $id = $request->request->get('id');
        $user = $this->getUser();
        $article = $em->getRepository('AdminBundle:Article')->findOneBy(array('id' => $id));
        $inLiker = false;
        foreach ($article->getLikers() as $liker) {
            if ($liker->getId() == $user->getId()) {
                $inLiker = true;
                break;
            }
        }
        if (!$inLiker) {
            $article->addLiker($user);
            $return['plus_one'] = true;
            $em->persist($article);
            $em->flush();
        }
        return new JsonResponse($return);
    }

    /**
     * Creates a new article entity.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/new", name="article_blog_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $article = new Article();
        $form = $this->createForm('AdminBundle\Form\ArticleType', $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $date = new DateTime();
            $article->setDateOpening($date);
            $em->persist($article);
            $em->flush($article);

            return $this->redirectToRoute('article_blog_show', array('id' => $article->getId()));
        }

        return $this->render('article_blog/new.html.twig', array(
                    'article' => $article,
                    'title' => 'Nouvel article',
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a article entity.
     *
     * @Route("/{id}", name="article_blog_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Article $article, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if ($user != null) {
            $inViewers = true;
            // Check if user in viewers
            $result = $em->getRepository('AdminBundle:Article')->userInViewers($user->getId(), $article->getId());
            if ($result == 0)
                $inViewers = false;
            else
                $inViewers = true;

            // endif

            if (!$inViewers)
                $article->addViewer($user);
            $em->persist($article);
            $em->flush();
        }

    

        return $this->render('article_blog/show.html.twig', array(
                    'article' => $article,
                   
        ));
    }

    /**
     * Displays a form to edit an existing article entity.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}/edit", name="article_blog_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Article $article) {
        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('AdminBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_blog_show', array('id' => $article->getId()));
        }

        return $this->render('article_blog/new.html.twig', array(
                    'article' => $article,
                    'title' => 'Edition de l\'article',
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a article entity.
     *  @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}", name="article_blog_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Article $article) {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush($article);
        }

        return $this->redirectToRoute('article_blog_index');
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('article_blog_delete', array('id' => $article->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
