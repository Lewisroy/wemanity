<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use Datetime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Page controller.
 *
 * @Route("/{_locale}/admin_manager/")
 */
class DefaultController extends Controller {
    /**
     * Lists all page admin entities.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("home/", name="home_admin")
     * @Method("GET")
     */
    public function homeAction() {

        return $this->render('AdminBundle:Default:home.html.twig');
    }

}
