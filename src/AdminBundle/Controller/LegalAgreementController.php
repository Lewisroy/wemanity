<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\LegalAgreement;
use AdminBundle\Entity\Page;
use AdminBundle\Form\LegalAgreementType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Page controller.
 *
 * @Route("/{_locale}/admin")
 */
class LegalAgreementController extends Controller {

    /**
     * Lists all page entities.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/privacy-policy/edit", name="admin_privacy_policy_edit")
     * @Method({"GET", "POST"})
     */
    public function PrivacyPolicyEditAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $legals = $em->getRepository('AdminBundle:LegalAgreement')->findBy(array(), null, 1);
        if (empty($legals)) {
            $legal = new LegalAgreement();
            $legal->setPrivacyPolicy("");
            $legal->setPrivacyPolicyEn("");
            $em->persist($legal);
            $em->flush();
            $legals[] = $legal;
        } else {
            $legal = $legals[0];
        }

        $form = $this->createForm(LegalAgreementType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $users = $em->getRepository('UserBundle:User')->findAll();
            foreach ($users as $user) {
                $user->setAgreePrivacyPolicy(true);
            }
            
            $text = $form->get('text')->getData();
            $textEN = $form->get('textEn')->getData();

            $legal->setPrivacyPolicy($text);
            $legal->setPrivacyPolicyEn($textEN);
            $em->persist($legal);
            $em->flush();


            return $this->redirectToRoute('admin_privacy_policy_edit');
        }
        $form->get('text')->setData($legal->getPrivacyPolicy());
        $form->get('textEn')->setData($legal->getPrivacyPolicyEn());

        return $this->render('agreement/new.html.twig', array(
                    'title' => 'Privacy Policy',
                    'form' => $form->createView()
        ));
    }

    /**
     * Lists all page entities.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/disclaimer/edit", name="admin_disclaimer_edit")
     * @Method({"GET", "POST"})
     */
    public function DisclaimerEditAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $legals = $em->getRepository('AdminBundle:LegalAgreement')->findBy(array(), null, 1);
        if (empty($legals)) {
            $legal = new LegalAgreement();
            $legal->setDisclaimer("");
            $legal->setDisclaimerEN("");
            $em->persist($legal);
            $em->flush();
            $legals[] = $legal;
        } else {
            $legal = $legals[0];
        }

        $form = $this->createForm(LegalAgreementType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $users = $em->getRepository('UserBundle:User')->findAll();
            foreach ($users as $user) {
                $user->setAgreeDisclaimer(true);
            }

            $text = $form->get('text')->getData();
            $textEN = $form->get('textEn')->getData();

            $legal->setDisclaimer($text);
            $legal->setDisclaimerEN($textEN);
            $em->persist($legal);
            $em->flush();


            return $this->redirectToRoute('admin_disclaimer_edit');
        }
        $form->get('text')->setData($legal->getDisclaimer());
        $form->get('textEn')->setData($legal->getDisclaimerEN());
        return $this->render('agreement/new.html.twig', array(
                    'title' => 'Disclaimer',
                    'form' => $form->createView()
        ));
    }

}
