<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Roles controller.
 *
 * @Route("/{_locale}/admin/")
 */
class RolesController extends Controller {

    /**
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("roles-management", name="admin_roles_management")
     * @Method("GET")
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findUserHaveRoles();

        return $this->render('AdminBundle:Roles:index.html.twig', [
                    'users' => $users
        ]);
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("roles-management-users", name="admin_roles_management_users")
     * @Method({"GET", "POST"})
     */
    public function indexUsersAction(Request $req) {

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('admin_roles_management_users'))
                ->setMethod('POST')
                ->getForm();
        $form->handleRequest($req);

        if ($form->isValid()) {
            $tags = $req->request->get('roles_management_list_users');
            $role = $req->request->get('roles_management_add_role');
            $tags = explode(",", $tags);

            foreach ($tags as $tag) {
                $manipulator = $this->get('fos_user.util.user_manipulator');

                if ($manipulator->addRole($tag, $role)) {
                    $this->addFlash('success', $this->get('translator')->trans('administration.roles.add_role_success', ['%role%' => $role, '%user%' => $tag]));
                } else {
                    $this->addFlash('warning', $this->get('translator')->trans('administration.roles.add_role_error', ['%user%' => $tag, '%role%' => $role]));
                }
            }
            return $this->redirectToRoute('admin_roles_management_users');
        }

        return $this->render('AdminBundle:Roles:index_users.html.twig', [
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("roles-management/add-role/{id}", name="admin_roles_management_add_role")
     * @Method("POST")
     */
    public function addRoleUserAction(Request $req, $id) {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('admin_roles_management_add_role', ['id' => $id]))
                ->setMethod('POST')
                ->getForm();
        $form->handleRequest($req);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($form->isValid() && $req->request->get('roles_management_add_role')) {
            $role = $req->request->get('roles_management_add_role');
            $manipulator = $this->get('fos_user.util.user_manipulator');

            if ($manipulator->addRole($user->getUsername(), $role)) {
                $this->addFlash('success', $this->get('translator')->trans('administration.roles.add_role_success', ['%role%' => $role, '%user%' => $user->getUsername()]));
            } else {
                $this->addFlash('warning', $this->get('translator')->trans('administration.roles.add_role_error', ['%user%' => $user->getUsername(), '%role%' => $role]));
            }

            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            return $response;
        } else {
            $content = $this->renderView('AdminBundle:Roles:add.html.twig', array(
                'user' => $user,
                'form' => $form->createView(),
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('global.add_role'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                'error' => true,
                    ])
            );
            return $response;
        }
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("roles-management/remove-role/{id}", name="admin_roles_management_remove_role")
     * @Method("POST")
     */
    public function removeRoleUserAction(Request $req, $id) {


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('admin_roles_management_remove_role', ['id' => $id]))
                ->setMethod('POST')
                ->getForm();
        $form->handleRequest($req);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($form->isValid() && $req->request->get('roles_management_remove_role')) {
            $role = $req->request->get('roles_management_remove_role');
            $manipulator = $this->get('fos_user.util.user_manipulator');

            if ($manipulator->removeRole($user->getUsername(), $role)) {
                $this->addFlash('success', $this->get('translator')->trans('administration.roles.remove_role_success', ['%role%' => $role, '%user%' => $user->getUsername()]));
            } else {
                $this->addFlash('warning', $this->get('translator')->trans('administration.roles.remove_role_error', ['%user%' => $user->getUsername(), '%role%' => $role]));
            }

            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            return $response;
        } else {
            $content = $this->renderView('AdminBundle:Roles:remove.html.twig', array(
                'user' => $user,
                'form' => $form->createView(),
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('global.remove_role'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                'error' => true,
                    ])
            );
            return $response;
        }
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("roles-management-autocomplete", name="admin_roles_management_autocomplete_user")
     * @Method({"POST"})
     */
    public function getAutocompleteUsersAction(Request $request) {
        
        $term = $request->request->get("term");
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository('UserBundle:User')->findAutocompleteUsers($term);

        return new JsonResponse($datas);
    }

}
