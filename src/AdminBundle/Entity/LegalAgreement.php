<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LegalAgreement
 *
 * @ORM\Entity
 * @ORM\Table(name="legal_agreement")
 */
class LegalAgreement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="privacy_policy", type="text")
     */
    private $privacyPolicy;

    /**
     * @var string
     *
     * @ORM\Column(name="privacy_policy_en", type="text")
     */
    private $privacyPolicyEn;

    /**
     * @var string
     *
     * @ORM\Column(name="disclaimer", type="text")
     */
    private $disclaimer;


    /**
     * @var string
     *
     * @ORM\Column(name="disclaimer_en", type="text")
     */
    private $disclaimerEN;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set privacyPolicy
     *
     * @param string $privacyPolicy
     *
     * @return LegalAgreement
     */
    public function setPrivacyPolicy($privacyPolicy)
    {
        $this->privacyPolicy = $privacyPolicy;

        return $this;
    }

    /**
     * Get privacyPolicy
     *
     * @return string
     */
    public function getPrivacyPolicy()
    {
        return $this->privacyPolicy;
    }

    /**
     * Set disclaimer
     *
     * @param string $disclaimer
     *
     * @return LegalAgreement
     */
    public function setDisclaimer($disclaimer)
    {
        $this->disclaimer = $disclaimer;

        return $this;
    }

    /**
     * Get disclaimer
     *
     * @return string
     */
    public function getDisclaimer()
    {
        return $this->disclaimer;
    }

    /**
     * Set privacyPolicyEn
     *
     * @param string $privacyPolicyEn
     *
     * @return LegalAgreement
     */
    public function setPrivacyPolicyEn($privacyPolicyEn)
    {
        $this->privacyPolicyEn = $privacyPolicyEn;

        return $this;
    }

    /**
     * Get privacyPolicyEn
     *
     * @return string
     */
    public function getPrivacyPolicyEn()
    {
        return $this->privacyPolicyEn;
    }

    /**
     * Set disclaimerEN
     *
     * @param string $disclaimerEN
     *
     * @return LegalAgreement
     */
    public function setDisclaimerEN($disclaimerEN)
    {
        $this->disclaimerEN = $disclaimerEN;

        return $this;
    }

    /**
     * Get disclaimerEN
     *
     * @return string
     */
    public function getDisclaimerEN()
    {
        return $this->disclaimerEN;
    }
}
