<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAppCommand extends Command
{
    protected function configure()
    {
       $this
        ->setName('update:app')
        ->setDescription('Pull yout git and update the app')
        ->setHelp('This command will pull your git before to update database and clear caches. You can an argument to merge a branch -b [branch name]')
        ->setDefinition(array(
                new InputArgument('branch', InputArgument::REQUIRED, 'The name branch to merge with'),
                new InputOption('b', null, InputOption::VALUE_NONE, 'If you want to merge'),
                new InputOption('c', null, InputOption::VALUE_NONE, 'Update the composer'),
            ));
    	;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Pulling git...........................');
        echo shell_exec('git pull');
        if ($input->getOption('b')) {
            $output->writeln('Merging.......................');
            echo shell_exec('git merge '.$input->getArgument('branch'));
        }
        if ($input->getOption('c')) {
            $output->writeln('Composer update.......................');
            echo shell_exec('php composer.phar update');
            echo shell_exec('composer update');
        }
    	$output->writeln('Update database.......................');
    	echo shell_exec('php bin/console doctrine:schema:update -f');
    	$output->writeln('Clear dev cache.......................');
    	echo shell_exec('php bin/console cache:clear -e dev');
    	$output->writeln('Clear prod cache......................');
    	echo shell_exec('php bin/console cache:clear -e prod');
    }
}