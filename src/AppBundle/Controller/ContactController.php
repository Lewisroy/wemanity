<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Contact controller.
 *
 * @Route("/{_locale}/contact")
 */
class ContactController extends Controller {

    /**
     * Lists all contact entities.
     * 
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/adminmanager/", name="contact_index")
     * @Method("GET")
     */
    public function indexAction() {
        
        $em = $this->getDoctrine()->getManager();

        $contacts = $em->getRepository('AppBundle:Contact')->findAll();

        return $this->render('contact/index.html.twig', array(
                    'contacts' => $contacts,
        ));
    }

    /**
     * Creates a new contact with the form that user will complete to contact administrator
     * @param Request $request  Get the request send by form
     * @return Contact $contact The contact to complete 
     * @return \Symfony\Component\Form\Form $form The form which will be complete by user
     * @Route("/new", name="contact_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $contact = new Contact();
        $form = $this->createForm('AppBundle\Form\ContactType', $contact);
        $form->handleRequest($request);
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $contact->setSubject($form->get('subject')->getData());
            $em->persist($contact);
            $em->flush($contact);
            return $this->render('contact/new.html.twig', array(
                        'contact' => $contact,
                        'form' => $form->createView(),
                        'user' => $user,
                        'message' => 'success'
            ));
            // return $this->redirectToRoute('au_general_homepage');
        }



        return $this->render('contact/new.html.twig', array(
                    'contact' => $contact,
                    'form' => $form->createView(),
                    'user' => $user
        ));
    }

    /**
     * Finds and displays a contact entity.
     * 
     * @param Contact $contact The entity to display
     * @return Contact $contact The entity to display
     * @return \Symfony\Component\Form\Form $deleteForm The delete form of the entity
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}", name="contact_show")
     * @Method("GET")
     */
    public function showAction(Contact $contact) {
    
        $deleteForm = $this->createDeleteForm($contact);

        return $this->render('contact/show.html.twig', array(
                    'contact' => $contact,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing contact entity.
     * 
     * @param Request $request Get the request send by form
     * @param Contact $contact The entity contact to edit
     * @return Contact $contact The entity contact to display
     * @return \Symfony\Component\Form\Form $editform The entity contact to edit
     * @return \Symfony\Component\Form\Form $deleteForm The entity contact to delete
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}/edit", name="contact_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Contact $contact) {
    
        $deleteForm = $this->createDeleteForm($contact);
        $editForm = $this->createForm('AppBundle\Form\ContactType', $contact);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact_edit', array('id' => $contact->getId()));
        }

        return $this->render('contact/edit.html.twig', array(
                    'contact' => $contact,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a contact entity.
     * 
     * @param Request $request Get the request send by form
     * @param Contact $contact The entity contact to delete
     * @return \Configuration\Route return the index of Contact
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}", name="contact_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Contact $contact) {
        
        $form = $this->createDeleteForm($contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contact);
            $em->flush();
        }
        return $this->redirectToRoute('contact_index');
    }

    /**
     * Creates a form to delete a contact entity.
     *
     * @param Contact $contact The contact entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Contact $contact) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('contact_delete', array('id' => $contact->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
