<?php

namespace AppBundle\Controller;

use AdminBundle\Entity\LegalAgreement;
use AdminBundle\Entity\Page;
use AdminBundle\Entity\ValidationChart;
use AdminBundle\Entity\PollAdmin;
use UserBundle\Entity\Message;
use function GuzzleHttp\Psr7\build_query;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    /**
     * @return  Root of the website, which redirect on /{_locale} 
     * @Route("/", name="wemanity_general_homepage")
     */
    public function indexAction() {
        return $this->redirectToRoute('wemanity_homepage');
    }

    /**
     * @return Artwork $artWorks the artworks to display 
     * @return Category $categories categories of the filters
     * @return Subcategory $subcategories subcategories for the filters
     * \Configuration\Route return the Discover Page
     * @Route("/{_locale}/", name="wemanity_homepage")
     */
    public function homeAction() {
        $em = $this->getDoctrine()->getManager();
        // utilisateur connecté.
        $user = $this->getUser();
    
        return $this->render('default/index.html.twig');
    }

    /**
     * @param Request $request
     * @return Text $privacyPolicy the Privacy Policy to display
     * @return Text $disclaimer the Disclaimer to display
     * @Route("/{_locale}/legals", name="general_legal_agreement")
     */
    public function LegalAgreementAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $locale = $request->getLocale();

        $legals = $em->getRepository('AdminBundle:LegalAgreement')->findBy(array(), null, 1);
        if (empty($legals)) {
            $legal = new LegalAgreement();
            $legal->setPrivacyPolicy("");
            $legal->setPrivacyPolicyEn("");
            $legal->setDisclaimer("");
            $legal->setDisclaimerEN("");
            $em->persist($legal);
            $em->flush();
            $legals[] = $legal;
        }
        if (strtolower($locale) == 'fr') {
            $legal = $legals[0];
            $privacyPolicy = $legal->getPrivacyPolicy();
            $disclaimer = $legal->getDisclaimer();
        } else {
            $legal = $legals[0];
            $privacyPolicy = $legal->getPrivacyPolicyEn();
            $disclaimer = $legal->getDisclaimerEN();
        }
        return $this->render('default/legals.html.twig', array(
                    'privacyPolicy' => $privacyPolicy,
                    'disclaimer' => $disclaimer
        ));
    }

    /**
     * We want to print the Page "About"
     * @param Request $request
     * @return Page $page The page we want to display
     * @Route("/{_locale}/about", name="au_about")
     */
    public function aboutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AdminBundle:Page')->findOneBy(array('name' => 'A propos'));
        return $this->render('default/page.html.twig', array('page' => $page));
    }

    /**
     * We want to print the Page "Help"
     * @param Request $request
     * @return Page $page The page we want to display
     * @Route("/{_locale}/help", name="au_help")
     */
    public function helpAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AdminBundle:Page')->findOneBy(array('name' => 'Aide'));
        return $this->render('default/page.html.twig', array('page' => $page));
    }

}
