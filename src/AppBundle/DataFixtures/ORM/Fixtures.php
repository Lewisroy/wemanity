<?php

namespace AppBundle\DataFixtures\ORM;

use ReportBundle\Entity\Reporting;
use ArtworkBundle\Entity\Artwork;
use ReportBundle\Entity\Subjects;
use AdminBundle\Entity\Article;
use LectureBundle\Entity\Category;
use LectureBundle\Entity\SubCategory;
use UserBundle\Entity\Activity;
use LectureBundle\Entity\Lecture;
use LectureBundle\Entity\Chapter;
use LectureBundle\Entity\Part;
use LectureBundle\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Fixtures implements FixtureInterface, ContainerAwareInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        $faker = \Faker\Factory::create();
        /*         * ****************************************************** */
        /*         * ***************** Global Fixtures ******************** */
        /*         * ****************************************************** */



        /*         * ************* Users ****************** */
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setUsername('admin');
        $user->setUsernameCanonical($user->getUsername());
        $user->setEmail($faker->email());
        $user->setEmailCanonical($user->getEmail());
        $user->setPlainPassword('admin123');
        $user->setFirstname($faker->firstName());
        $user->setLastname($faker->lastName());
        $user->setRoles(array('ROLE_SUPER_ADMIN'));
        $userManager->updateUser($user, true);

        for ($i = 0; $i < 20; $i++) {
            $user = $userManager->createUser();
            $user->setEnabled(true);
            $user->setUsername('test_' . $faker->userName());
            $user->setUsernameCanonical($user->getUsername());
            $user->setEmail($faker->email());
            $user->setEmailCanonical($user->getEmail());
            $user->setPlainPassword('test123');
            $user->setFirstname($faker->firstName());
            $user->setLastname($faker->lastName());
            $user->setRoles(array('ROLE_USER'));
            $userManager->updateUser($user, true);
        }

    }
}
