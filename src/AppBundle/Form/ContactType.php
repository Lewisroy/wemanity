<?php

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array('label' => 'contact.email', 'required' => true))
                ->add('subject', ChoiceType::class, array(
                'choices'  => array(
                    'contact.subject.choice1' =>'contact.subject.choice1',
                    'contact.subject.choice1' => 'contact.subject.choice1',
                    'contact.subject.choice1' => 'contact.subject.choice1',
                    'contact.subject.choice1' =>  'contact.subject.choice1')))
                ->add('message', TextareaType::class, array('label' => 'contact.message', 'required' => true))        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Contact'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'au_generalbundle_contact';
    }


}
