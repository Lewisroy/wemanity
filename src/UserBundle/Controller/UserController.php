<?php

namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Services\UserServices;
use UserBundle\Entity\User;
use UserBundle\Form\UserType;

/**
 * User controller.
 *
 * @Route("/{_locale}/user")
 */
class UserController extends Controller {

    
    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}/profile", name="show_me")
     * @Method("GET")
     */
    public function showMeAction(User $user) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        if ($user->getIsBanni()) {
            throw $this->createNotFoundException();
        }

        $currentUser = $this->getUser();
        $followers = $this->getDoctrine()->getRepository('UserBundle:User')->isFollowers($currentUser, $user);

        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
                    'user' => $user,
                    'delete_form' => $deleteForm->createView(),
                    'isFollowers' => $followers['nb'],
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST", "HEAD"})
     */
    public function editAction(Request $request, User $user) {

        $em = $this->getDoctrine()->getManager();

        $picture = $user->getPicture();
        $user = $em->getRepository('UserBundle:User')->findOneBy(array('id' => $user->getId()));

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        if ($user->getId() != $this->getUser()->getId() && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('UserBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($editForm->get('picture')->getData() != null) {
                $file = $editForm->get('picture')->getData();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                        'uploads/users/' . $user->getId(), $fileName
                );
                $user->setPicture($fileName);
            } else {
                $user->setPicture($picture);
            }

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('show_me', array('id' => $user->getId()));
        }

        return $this->render('user/edit.html.twig', array(
                    'user' => $user,
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to change parameters an existing User entity.
     *
     * @Route("/{id}/parameters", name="user_parameters")
     * @Method({"GET", "POST"})
     */
    public function parametersAction(Request $request, User $user) {

        $em = $this->getDoctrine()->getManager();

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        if ($user->getId() != $this->getUser()->getId() && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }


        return $this->render('user/parameters.html.twig', array(
                    'user' => $user,
        ));
    }

    

}
