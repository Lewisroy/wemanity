<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", nullable = true)
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname_status", type="string", nullable = true)
     */
    protected $firstnameStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", nullable = true)
     */
    protected $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname_status", type="string", nullable = true)
     */
    protected $lastnameStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="email_status", type="string", nullable = true)
     */
    protected $emailStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable = true)
     */
    protected $birthday;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday_status", type="string", nullable = true)
     */
    protected $birthday_status;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable = true)
     */
    protected $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="gender_status", type="string", nullable = true)
     */
    protected $genderStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", nullable = true)
     */
    protected $language;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", nullable = true)
     */
    protected $street;

    /**
     * @var string
     *
     * @ORM\Column(name="street_status", type="string", nullable = true)
     */
    protected $streetStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="postal_code", type="integer", nullable = true)
     */
    protected $postalCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="postal_code_status", type="string", nullable = true)
     */
    protected $postalCodeStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", nullable = true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="city_status", type="string", nullable = true)
     */
    protected $cityStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", nullable = true)
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(name="country_status", type="string", nullable = true)
     */
    protected $countryStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", nullable = true)
     */
    protected $location;

    /**
     * @var text
     *
     * @ORM\Column(name="styles", type="text", nullable = true)
     */
    protected $styles;

    /**
     * @var text
     *
     * @ORM\Column(name="styles_status", type="text", nullable = true)
     */
    protected $stylesStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="tools", type="text", nullable = true)
     */
    protected $tools;

    /**
     * @var text
     *
     * @ORM\Column(name="tools_status", type="text", nullable = true)
     */
    protected $toolsStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="equipments", type="text", nullable = true)
     */
    protected $equipments;

    /**
     * @var text
     *
     * @ORM\Column(name="equipments_status", type="text", nullable = true)
     */
    protected $equipmentsStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="hobbies", type="text", nullable = true)
     */
    protected $hobbies;

    /**
     * @var text
     *
     * @ORM\Column(name="hobbies_status", type="text", nullable = true)
     */
    protected $hobbiesStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", nullable = true)
     */
    protected $website;

    /**
     * @var text
     *
     * @ORM\Column(name="bio", type="text", nullable = true)
     */
    protected $bio;

    /**
     * @var string
     *
     * @ORM\Column(name="socialmedia", type="string", nullable = true)
     */
    protected $socialmedia;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", nullable = true)
     */
    protected $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", nullable = true)
     */
    protected $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", nullable = true)
     */
    protected $instagram;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin", type="string", nullable = true)
     */
    protected $linkedin;

    /**
     * @var string
     *
     * @ORM\Column(name="pinterest", type="string", nullable = true)
     */
    protected $pinterest;

    /**
     * @var string
     *
     * @ORM\Column(name="google_plus", type="string", nullable = true)
     */
    protected $googlePlus;

    /**
     * @var text
     *
     * @ORM\Column(name="studies", type="text", nullable = true)
     */
    protected $studies;

    /**
     * @var text
     *
     * @ORM\Column(name="studies_status", type="text", nullable = true)
     */
    protected $studiesStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="skills", type="text", nullable = true)
     */
    protected $skills;

    /**
     * @var text
     *
     * @ORM\Column(name="skills_status", type="text", nullable = true)
     */
    protected $skillsStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="experience", type="text", nullable = true)
     */
    protected $experiences;

    /**
     * @var text
     *
     * @ORM\Column(name="experience_status", type="text", nullable = true)
     */
    protected $experiencesStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="my_projects", type="text", nullable = true)
     */
    protected $myProjects;

    /**
     * @var text
     *
     * @ORM\Column(name="visited_country", type="text", nullable = true)
     */
    protected $visiteCountry;

    /**
     * @var text
     *
     * @ORM\Column(name="talking_travels", type="text", nullable = true)
     */
    protected $talkingTravels;

    /**
     * @var text
     *
     * @ORM\Column(name="my_work", type="text", nullable = true)
     */
    protected $my_work;

    /**
     * @var boolean
     *
     * @ORM\Column(name="profile_type", type="boolean", nullable = true)
     */
    protected $profileType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="defy_me", type="boolean", nullable = true)
     */
    protected $defyMe;

    /**
     * @Assert\File(maxSize="2048k")
     * @Assert\Image(mimeTypesMessage="Please upload a valid image.")
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $picture;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
    }

   
    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname) {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday($birthday) {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday() {
        return $this->birthday;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender) {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender() {
        return $this->gender;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return User
     */
    public function setLanguage($language) {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return User
     */
    public function setLocation($location) {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set styles
     *
     * @param $styles
     *
     * @return User
     */
    public function setStyles($styles) {
        $this->styles = $styles;

        return $this;
    }

    /**
     * Get styles
     *
     * @return string
     */
    public function getStyles() {
        return $this->styles;
    }

    /**
     * Set tools
     *
     * @param string $tools
     *
     * @return User
     */
    public function setTools($tools) {
        $this->tools = $tools;

        return $this;
    }

    /**
     * Get tools
     *
     * @return string
     */
    public function getTools() {
        return $this->tools;
    }

    /**
     * Set equipments
     *
     * @param string $equipments
     *
     * @return User
     */
    public function setEquipments($equipments) {
        $this->equipments = $equipments;

        return $this;
    }

    /**
     * Get equipments
     *
     * @return string
     */
    public function getEquipments() {
        return $this->equipments;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return User
     */
    public function setWebsite($website) {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite() {
        return $this->website;
    }

    /**
     * Set bio
     *
     * @param string $bio
     *
     * @return User
     */
    public function setBio($bio) {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio() {
        return $this->bio;
    }

    /**
     * Set socialmedia
     *
     * @param string $socialmedia
     *
     * @return User
     */
    public function setSocialmedia($socialmedia) {
        $this->socialmedia = $socialmedia;

        return $this;
    }

    /**
     * Get socialmedia
     *
     * @return string
     */
    public function getSocialmedia() {
        return $this->socialmedia;
    }

    /**
     * Set profileType
     *
     * @param boolean $profileType
     *
     * @return User
     */
    public function setProfileType($profileType) {
        $this->profileType = $profileType;

        return $this;
    }

    /**
     * Get profileType
     *
     * @return boolean
     */
    public function getProfileType() {
        return $this->profileType;
    }

    /**
     * Set defyMe
     *
     * @param boolean $defyMe
     *
     * @return User
     */
    public function setDefyMe($defyMe) {
        $this->defyMe = $defyMe;

        return $this;
    }

    /**
     * Get defyMe
     *
     * @return boolean
     */
    public function getDefyMe() {
        return $this->defyMe;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getPicture() {
        return $this->picture;
    }

    /**
     * Set picture
     *
     * @param string $picture
     *
     * @return User
     */
    public function setPicture($picture) {
        $this->picture = $picture;

        return $this;
    }

    

    /**
     * Set street
     *
     * @param string $street
     *
     * @return User
     */
    public function setStreet($street) {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet() {
        return $this->street;
    }

    /**
     * Set postalCode
     *
     * @param integer $postalCode
     *
     * @return User
     */
    public function setPostalCode($postalCode) {
        $this->postalCode = $postalCode;
    }

    /**
     * Set typeArtsien
     *
     * @param array $typeArtsien
     *
     * @return User
     */
    public function setTypeArtsien($typeArtsien) {
        $this->typeArtsien = $typeArtsien;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return integer
     */
    public function getPostalCode() {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

   

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set hobbies
     *
     * @param string $hobbies
     *
     * @return User
     */
    public function setHobbies($hobbies) {
        $this->hobbies = $hobbies;

        return $this;
    }

    /**
     * Get hobbies
     *
     * @return string
     */
    public function getHobbies() {
        return $this->hobbies;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return User
     */
    public function setFacebook($facebook) {
        $this->facebook = $facebook;
    }

    /**
     * Get activeBoutique
     *
     * @return boolean
     */
    public function getActiveBoutique() {
        return $this->activeBoutique;
    }


    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook() {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return User
     */
    public function setTwitter($twitter) {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter() {
        return $this->twitter;
    }

    /**
     * Set instagram
     *
     * @param string $instagram
     *
     * @return User
     */
    public function setInstagram($instagram) {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram() {
        return $this->instagram;
    }

    /**
     * Set linkedin
     *
     * @param string $linkedin
     *
     * @return User
     */
    public function setLinkedin($linkedin) {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * Get linkedin
     *
     * @return string
     */
    public function getLinkedin() {
        return $this->linkedin;
    }

    /**
     * Set googlePlus
     *
     * @param string $googlePlus
     *
     * @return User
     */
    public function setGooglePlus($googlePlus) {
        $this->googlePlus = $googlePlus;

        return $this;
    }

    /**
     * Get googlePlus
     *
     * @return string
     */
    public function getGooglePlus() {
        return $this->googlePlus;
    }

    /**
     * Set studies
     *
     * @param string $studies
     *
     * @return User
     */
    public function setStudies($studies) {
        $this->studies = $studies;

        return $this;
    }

    /**
     * Get studies
     *
     * @return string
     */
    public function getStudies() {
        return $this->studies;
    }

    /**
     * Set skills
     *
     * @param string $skills
     *
     * @return User
     */
    public function setSkills($skills) {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return string
     */
    public function getSkills() {
        return $this->skills;
    }

    /**
     * Set experiences
     *
     * @param string $experiences
     *
     * @return User
     */
    public function setExperiences($experiences) {
        $this->experiences = $experiences;

        return $this;
    }

    /**
     * Get experiences
     *
     * @return string
     */
    public function getExperiences() {
        return $this->experiences;
    }

    /**
     * Set myProjects
     *
     * @param string $myProjects
     *
     * @return User
     */
    public function setMyProjects($myProjects) {
        $this->myProjects = $myProjects;

        return $this;
    }

    /**
     * Get myProjects
     *
     * @return string
     */
    public function getMyProjects() {
        return $this->myProjects;
    }

    /**
     * Set visiteCountry
     *
     * @param string $visiteCountry
     *
     * @return User
     */
    public function setVisiteCountry($visiteCountry) {
        $this->visiteCountry = $visiteCountry;

        return $this;
    }

    /**
     * Get visiteCountry
     *
     * @return string
     */
    public function getVisiteCountry() {
        return $this->visiteCountry;
    }

    /**
     * Set talkingTravels
     *
     * @param string $talkingTravels
     *
     * @return User
     */
    public function setTalkingTravels($talkingTravels) {
        $this->talkingTravels = $talkingTravels;

        return $this;
    }

    /**
     * Get talkingTravels
     *
     * @return string
     */
    public function getTalkingTravels() {
        return $this->talkingTravels;
    }

    /**
     * Set myWork
     *
     * @param string $myWork
     *
     * @return User
     */
    public function setMyWork($myWork) {
        $this->my_work = $myWork;

        return $this;
    }

    /**
     * Get myWork
     *
     * @return string
     */
    public function getMyWork() {
        return $this->my_work;
    }

    /**
     * Set firstnameStatus
     *
     * @param string $firstnameStatus
     *
     * @return User
     */
    public function setFirstnameStatus($firstnameStatus) {
        $this->firstnameStatus = $firstnameStatus;

        return $this;
    }

    /**
     * Get firstnameStatus
     *
     * @return string
     */
    public function getFirstnameStatus() {
        return $this->firstnameStatus;
    }

    /**
     * Set lastnameStatus
     *
     * @param string $lastnameStatus
     *
     * @return User
     */
    public function setLastnameStatus($lastnameStatus) {
        $this->lastnameStatus = $lastnameStatus;

        return $this;
    }

    /**
     * Get lastnameStatus
     *
     * @return string
     */
    public function getLastnameStatus() {
        return $this->lastnameStatus;
    }

    /**
     * Set birthdayStatus
     *
     * @param \DateTime $birthdayStatus
     *
     * @return User
     */
    public function setBirthdayStatus($birthdayStatus) {
        $this->birthday_status = $birthdayStatus;

        return $this;
    }

    /**
     * Get birthdayStatus
     *
     * @return \DateTime
     */
    public function getBirthdayStatus() {
        return $this->birthday_status;
    }

    /**
     * Set genderStatus
     *
     * @param string $genderStatus
     *
     * @return User
     */
    public function setGenderStatus($genderStatus) {
        $this->genderStatus = $genderStatus;

        return $this;
    }

    /**
     * Get genderStatus
     *
     * @return string
     */
    public function getGenderStatus() {
        return $this->genderStatus;
    }

    /**
     * Set streetStatus
     *
     * @param string $streetStatus
     *
     * @return User
     */
    public function setStreetStatus($streetStatus) {
        $this->streetStatus = $streetStatus;

        return $this;
    }

    /**
     * Get streetStatus
     *
     * @return string
     */
    public function getStreetStatus() {
        return $this->streetStatus;
    }

    /**
     * Set postalCodeStatus
     *
     * @param integer $postalCodeStatus
     *
     * @return User
     */
    public function setPostalCodeStatus($postalCodeStatus) {
        $this->postalCodeStatus = $postalCodeStatus;

        return $this;
    }

    /**
     * Get postalCodeStatus
     *
     * @return integer
     */
    public function getPostalCodeStatus() {
        return $this->postalCodeStatus;
    }

    /**
     * Set cityStatus
     *
     * @param string $cityStatus
     *
     * @return User
     */
    public function setCityStatus($cityStatus) {
        $this->cityStatus = $cityStatus;

        return $this;
    }

    /**
     * Get cityStatus
     *
     * @return string
     */
    public function getCityStatus() {
        return $this->cityStatus;
    }

    /**
     * Set countryStatus
     *
     * @param string $countryStatus
     *
     * @return User
     */
    public function setCountryStatus($countryStatus) {
        $this->countryStatus = $countryStatus;

        return $this;
    }

    /**
     * Get countryStatus
     *
     * @return string
     */
    public function getCountryStatus() {
        return $this->countryStatus;
    }

    /**
     * Set stylesStatus
     *
     * @param string $stylesStatus
     *
     * @return User
     */
    public function setStylesStatus($stylesStatus) {
        $this->stylesStatus = $stylesStatus;

        return $this;
    }

    /**
     * Get stylesStatus
     *
     * @return string
     */
    public function getStylesStatus() {
        return $this->stylesStatus;
    }

    /**
     * Set toolsStatus
     *
     * @param string $toolsStatus
     *
     * @return User
     */
    public function setToolsStatus($toolsStatus) {
        $this->toolsStatus = $toolsStatus;

        return $this;
    }

    /**
     * Get toolsStatus
     *
     * @return string
     */
    public function getToolsStatus() {
        return $this->toolsStatus;
    }

    /**
     * Set equipmentsStatus
     *
     * @param string $equipmentsStatus
     *
     * @return User
     */
    public function setEquipmentsStatus($equipmentsStatus) {
        $this->equipmentsStatus = $equipmentsStatus;

        return $this;
    }

    /**
     * Get equipmentsStatus
     *
     * @return string
     */
    public function getEquipmentsStatus() {
        return $this->equipmentsStatus;
    }

    /**
     * Set hobbiesStatus
     *
     * @param string $hobbiesStatus
     *
     * @return User
     */
    public function setHobbiesStatus($hobbiesStatus) {
        $this->hobbiesStatus = $hobbiesStatus;

        return $this;
    }

    /**
     * Get hobbiesStatus
     *
     * @return string
     */
    public function getHobbiesStatus() {
        return $this->hobbiesStatus;
    }

    /**
     * Set studiesStatus
     *
     * @param string $studiesStatus
     *
     * @return User
     */
    public function setStudiesStatus($studiesStatus) {
        $this->studiesStatus = $studiesStatus;

        return $this;
    }

    /**
     * Get studiesStatus
     *
     * @return string
     */
    public function getStudiesStatus() {
        return $this->studiesStatus;
    }

    /**
     * Set skillsStatus
     *
     * @param string $skillsStatus
     *
     * @return User
     */
    public function setSkillsStatus($skillsStatus) {
        $this->skillsStatus = $skillsStatus;

        return $this;
    }

    /**
     * Get skillsStatus
     *
     * @return string
     */
    public function getSkillsStatus() {
        return $this->skillsStatus;
    }

    /**
     * Set experiencesStatus
     *
     * @param string $experiencesStatus
     *
     * @return User
     */
    public function setExperiencesStatus($experiencesStatus) {
        $this->experiencesStatus = $experiencesStatus;

        return $this;
    }

    /**
     * Get experiencesStatus
     *
     * @return string
     */
    public function getExperiencesStatus() {
        return $this->experiencesStatus;
    }

    /**
     * Set pinterest
     *
     * @param string $pinterest
     *
     * @return User
     */
    public function setPinterest($pinterest) {
        $this->pinterest = $pinterest;

        return $this;
    }

    /**
     * Get pinterest
     *
     * @return string
     */
    public function getPinterest() {
        return $this->pinterest;
    }

    /**
     * Set emailStatus
     *
     * @param string $emailStatus
     *
     * @return User
     */
    public function setEmailStatus($emailStatus) {
        $this->emailStatus = $emailStatus;

        return $this;
    }

    /**
     * Get emailStatus
     *
     * @return string
     */
    public function getEmailStatus() {
        return $this->emailStatus;
    }

}
